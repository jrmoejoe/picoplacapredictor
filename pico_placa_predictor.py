# Author: Jose Davalos
# Description: Simple Pico y Placa predictor program. it uses the license plate number, date and time to determine
# if a vehicle is under Pico y Placa rules
import datetime


class PicoPlacaPredictor:

	#constructor for class -> with some default values
	def __init__(self, license_number=0, week_day=0):
		self.license_number = license_number
		self.week_day = week_day

	# this function will get user keyboard input and return each requirement.
	def get_user_input(self):
		license_plate = input("Enter licence plate: ")
		date = input("Enter a date in the format (YYYY-MM-DD): ")
		time = input("What time is it? (format: HH:MM) ")
		return license_plate, date, time

	# this function will return if The vehicle is under 'Pico y Placa' rules or not.
	# time check will be required.
	def pico_placa_predictor(self):
		# we check the last number of the license plate and the day of the week
		if (self.license_number is 1 or self.license_number is 2) and week_day is 0:
			return 1
		if (self.license_number is 3 or self.license_number is 4) and week_day is 1:
			return 1
		if (self.license_number is 5 or self.license_number is 6) and week_day is 2:
			return 1
		if (self.license_number is 7 or self.license_number is 8) and week_day is 3:
			return 1
		if (self.license_number is 9 or self.license_number is 0) and week_day is 4:
			return 1

		if week_day is 5:
			print("No \'Pico y Placa\' for this day. ")
			return 0
		if week_day is 6:
			print("No \'Pico y Placa\' for this day. ")
			return 0

if __name__ == '__main__':
	print("Welcome to the \'Pico y Placa\' predictor!")
	pp = PicoPlacaPredictor()
	run = True
	while run:

		license_plate, date, time = pp.get_user_input()
		# first, we need to figure out which day of the week is the given date
		# datetime.weekday() will Return the day of the week as an integer, where Monday is 0 and Sunday is 6.
		try:
			datetime_str = date + " " + time
			datetime_obj = datetime.datetime.strptime(datetime_str, '%Y-%m-%d %H:%M')
		except ValueError:
			print("Date format or Time format input is wrong.. please use (YYYY-MM-DD) and (HH:MM)")
			continue

		year = datetime_obj.date().year
		month = datetime_obj.date().month
		day = datetime_obj.date().day

		#since we are pre-processing variables, we can do it here as well
		#Last number of license plate
		try:
			last_license_number = int(license_plate[-1:])
		except ValueError:
			print("You're license plate may be wrong...")
			continue

		# Get week day
		week_day = datetime.datetime(year, month, day).weekday()

		# we call our predictor method
		pp = PicoPlacaPredictor(last_license_number, week_day)
		is_in_pico_placa = pp.pico_placa_predictor()

		# datetime object for the morning
		datetime_obj_seven = datetime.datetime.strptime("07:00", '%H:%M')
		datetime_obj_nine = datetime.datetime.strptime("09:30", '%H:%M')

		# datetime object for the afternoon
		datetime_obj_four = datetime.datetime.strptime("16:00", '%H:%M')
		datetime_obj_seven_thirty = datetime.datetime.strptime("19:30", '%H:%M')
		# Day and Time check
		if is_in_pico_placa is 1:
			if datetime_obj_seven.time() < datetime_obj.time() < datetime_obj_nine.time():
				print(f"The vehicle with license {license_plate} will be under 'Pico y Placa' restrictions.")
			elif datetime_obj_four.time() < datetime_obj.time() < datetime_obj_seven_thirty.time():
				print(f"The vehicle with license {license_plate} will be under 'Pico y Placa' restrictions.")
			else:
				print(f"The vehicle with license {license_plate} will NOT be under 'Pico y Placa' restrictions.")
		else:
			print(f"The vehicle with license {license_plate} will NOT be under 'Pico y Placa' restrictions.")

		cont = str(input("Continue? (Y/N): "))
		if cont is "N" or cont is "n":
			run = False



	print()
	print("End of Program")