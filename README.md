PicoPlacaPredictor

Requirements:
 - The program inputs:
    - Complete license plate number.
    - A date as String
    - Time of the day
 - Program Output:
    - The vehicle will be under 'Pico y Placa' rules or not.

- Program Excecution example:

        Welcome to the 'Pico y Placa' predictor!
        
        Enter licence plate: PPP-1111
        
        Enter a date in the format (YYYY-MM-DD): 2019-11-25
        
        What time is it? (format: HH:MM) 8:30
        
        >> The vehicle with license PPP-1111 will be under 'Pico y Placa' restrictions. <<
        
        Continue? (Y/N): Y
        
        Enter licence plate: PPP-1111
        
        Enter a date in the format (YYYY-MM-DD): 2019-11-26
        
        What time is it? (format: HH:MM) 17:15
        
        >> The vehicle with license PPP-1111 will NOT be under 'Pico y Placa' restrictions. <<
        
        Continue? (Y/N): N
        
        End of Program
